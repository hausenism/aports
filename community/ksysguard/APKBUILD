# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksysguard
pkgver=5.20.2
pkgrel=0
pkgdesc="Track and control the processes running in your system"
# armhf blocked by extra-cmake-modules
# s390x blocked by libksysguard
arch="all !armhf !s390x"
url="https://userbase.kde.org/KSysGuard"
license="GPL-2.0-only"
makedepends="extra-cmake-modules kconfig-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev ki18n-dev kiconthemes-dev kinit-dev kitemviews-dev kio-dev knewstuff-dev knotifications-dev kwindowsystem-dev libksysguard-dev"
source="https://download.kde.org/stable/plasma/$pkgver/ksysguard-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# ksystemstatstest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "ksystemstatstest"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="904b969b765e2cf4a229256e2a13c3c4177f08071d3b26f0a267b2b4bdcfe74a3098aaad2dec41db702546582f7e2e6ab488c49275ac13564956795c86e2d905  ksysguard-5.20.2.tar.xz"
