# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-plymouth
pkgver=5.20.2
pkgrel=0
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/"
pkgdesc="Breeze theme for Plymouth"
license="GPL-2.0-or-later"
depends="plymouth"
makedepends="extra-cmake-modules plymouth-dev"
source="https://download.kde.org/stable/plasma/$pkgver/breeze-plymouth-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DDISTRO_NAME="$(grep PRETTY_NAME /etc/os-release | awk -F  = '{ print $2 }')" \
		-DDISTRO_VERSION="$(grep VERSION_ID /etc/os-release | awk -F  = '{ print $2 }')"
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="58d3d5e5f3d3499cb5f57d24ca18b03c27f5b662d60b17b1e5677b60100da41765acb69df5492491b1fe8ab2a5b2aed8958dc413be94983a0bc95a7baa5d1b20  breeze-plymouth-5.20.2.tar.xz"
