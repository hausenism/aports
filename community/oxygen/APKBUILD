# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=oxygen
pkgver=5.20.2
pkgrel=0
pkgdesc="Artwork, styles and assets for the Oxygen visual style for the Plasma Desktop"
# armhf blocked by extra-cmake-modules
# s390x blocked by frameworkintegration
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="LGPL-2.1-or-later"
makedepends="extra-cmake-modules qt5-qtbase-dev ki18n-dev kconfig-dev kguiaddons-dev kwidgetsaddons-dev kservice-dev kcompletion-dev frameworkintegration-dev kwindowsystem-dev xcb-util-dev kwayland-dev kcmutils-dev kdecoration-dev"
source="https://download.kde.org/stable/plasma/$pkgver/oxygen-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="12bb826f0b50f4a4f77bea463028f65538aebb56bec117f73ead20935e55667d391095616bae618cf149d3244871c6b645d17408e59264f113f450e37624dc73  oxygen-5.20.2.tar.xz"
