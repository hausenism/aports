# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwin
pkgver=5.20.2
pkgrel=0
pkgdesc="An easy to use, but flexible, composited Window Manager"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by kscreenlocker
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only"
depends="qt5-qtwayland qt5-qtmultimedia kirigami2 xorg-server-xwayland"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtscript-dev qt5-qtsensors-dev qt5-qtx11extras-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev kglobalaccel-dev ki18n-dev kinit-dev knotifications-dev kpackage-dev plasma-framework-dev kwidgetsaddons-dev kwindowsystem-dev kiconthemes-dev kidletime-dev kwayland-dev kcompletion-dev kdeclarative-dev kcmutils-dev kio-dev ktextwidgets-dev knewstuff-dev kservice-dev kxmlgui-dev kactivities-dev kdecoration-dev kscreenlocker-dev breeze-dev libepoxy-dev mesa-dev wayland-dev xcb-util-cursor-dev xcb-util-image-dev xcb-util-wm-dev libinput-dev eudev-dev libdrm-dev mesa-gbm fontconfig-dev libxkbcommon-dev libxi-dev kwayland-server-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev qt5-qttools-static kdoctools-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/plasma/$pkgver/kwin-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install

	# kwin_wayland has CAP_SYS_NICE set. Because of this, libdbus doesn't trust the
	# environment and ignores it, causing for example keyboard shortcuts to not work
	# Remove CAP_SYS_NICE from kwin_wayland to make them work again
	setcap -r "$pkgdir"/usr/bin/kwin_wayland
}
sha512sums="bd92075a2ebec2f3c905e882f61b6d8033e5fc0c565bfb765f4dd66e940439f03cab3062093c45b52a26935c9d88d3827c0311f0f7cca100f20ef91fd7a73afb  kwin-5.20.2.tar.xz"
