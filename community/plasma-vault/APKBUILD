# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-vault
pkgver=5.20.2
pkgrel=0
pkgdesc="Plasma applet and services for creating encrypted vaults"
# armhf blocked by extra-cmake-modules
# s390x blocked by libksysguard
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1-only AND LGPL-3.0-only)"
makedepends="extra-cmake-modules qt5-qtbase-dev kactivities-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev ki18n-dev kio-dev plasma-framework-dev kwidgetsaddons-dev networkmanager-qt-dev libksysguard-dev"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-vault-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="e4b264475648c0f484bad3c8f83e90c3a7df6d119d548b7f81966db248934236e7f205466fafa8f17734fd6f377d3ab1ae343721f8a3ad3fd5e40df49ba9841f  plasma-vault-5.20.2.tar.xz"
